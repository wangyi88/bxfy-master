package com.bxfy.store.service.api;

import java.util.Date;

/**
 * @author: 王毅
 * @Date: 2019-01-13 13:22
 * @Description: 库存接口
 */
public interface StoreServiceApi {


    /**
     * @author: 王毅
     * @date: 2019-01-13 13:27
     * 功能描述: 根据商品ID和查询版本
     */
    int selectVersion(String supplierId,String goodsId);

    /**
     * @author: 王毅
     * @date: 2019-01-13 13:27
     * 功能描述:更新库存
     */
    int updateStoreCountByVersion(int version, String goodsId, String updateBy, Date updateTime);

    
    /**
     * @author: 王毅
     * @date: 2019-01-13 13:28
     * 功能描述:
     */
    int selectStoreCount(String supplierId,String goodsId);

}
