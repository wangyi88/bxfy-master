package com.bxfy.store.service.api;


import com.bxfy.store.service.api.model.Order;
import com.bxfy.store.service.api.utils.PageList;

/**
 * @author 王毅
 * 接口
 */
public interface HelloServiceApi {

   public String sayHello(String name);

   public PageList<Order> getOrderInfo(String name);

}
