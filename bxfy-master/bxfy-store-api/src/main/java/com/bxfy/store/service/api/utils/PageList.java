package com.bxfy.store.service.api.utils;

import java.io.Serializable;
import java.util.List;

public class PageList<T> implements Serializable {
    private static final long serialVersionUID = -1428187732936978938L;
    private long totalCount;
    private int start;
    private int limit;
    private List<T> root;

    public PageList() {
    }

    private PageList(long totalCount, int start, int limit, List<T> root) {
        this.totalCount = totalCount;
        this.start = start;
        this.limit = limit;
        this.root = root;
    }

    public static <T> PageList<T> createPageList(int start, int limit) {
        return new PageList(0L, start, limit, (List)null);
    }

    public static <T> PageList<T> createPageList(List<T> root, long totalCount) {
        return new PageList(totalCount, 0, 0, root);
    }

    public long getTotalCount() {
        return this.totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public int getStart() {
        return this.start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLimit() {
        return this.limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public List<T> getRoot() {
        return this.root;
    }

    public void setRoot(List<T> root) {
        this.root = root;
    }
}
