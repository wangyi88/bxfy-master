package com.bxfy.order;

import lombok.Builder;
import lombok.Data;

/**
 * @Auther: 王毅
 * @Date: 2019-01-11 21:36
 * @Description:
 */
@Data
@Builder
public class Approval {

    private String code;

    private String desc;

}
