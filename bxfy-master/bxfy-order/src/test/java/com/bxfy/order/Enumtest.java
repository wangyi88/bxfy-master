package com.bxfy.order;

import com.bxfy.order.utils.enums.MyEnum;
import org.apache.commons.lang3.EnumUtils;

/**
 * @author 王毅
 * @date 2019-01-21 15:43
 * @description 枚举类测试类
 */
public class Enumtest {

    public static void main(String[] args) {
        if(EnumUtils.isValidEnum(MyEnum.class, "appA")){
            System.out.println("包含");
        }else {
            System.out.println("不包含");
        }
    }
}
