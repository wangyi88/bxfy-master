package com.bxfy.order.config.exception;

import lombok.Data;

/**
 * @author wangyi
 * 自定义异常类
 * 2018-12-13
 */
@Data
public class BusinessException extends RuntimeException{

    private int code;

    private String msg;

    public BusinessException(int code, String msg){
        this.code=code;
        this.msg = msg;
    }

}
