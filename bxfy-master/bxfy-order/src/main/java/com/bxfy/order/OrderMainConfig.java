package com.bxfy.order;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author 王毅
 * 配置中心
 */
@Configuration
@ComponentScan(basePackages = {"com.bxfy.order.*"})
@MapperScan(basePackages = {"com.bxfy.order.dao"})
public class OrderMainConfig {

}
