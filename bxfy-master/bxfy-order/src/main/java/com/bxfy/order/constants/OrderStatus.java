package com.bxfy.order.constants;

/**
 * @Auther: 王毅
 * @Date: 2019-01-13 11:35
 * @Description: 订单状态 枚举类
 */
public enum OrderStatus {

    ORDER_CREATED("1"),

    ORDER_PAYED("2"),

    ORDER_FAIL("3");

    private String status;

    private  OrderStatus(String status){
        this.status=status;
    };

    public String getValue(){
        return status;
    }


}
