package com.bxfy.order.service;

/**
 * @author: 王毅
 * @date: 2019-01-12 23:08
 * 功能描述:
 */
public interface ListFilterStrategy<T> {

    public boolean contain(T t);

}
