package com.bxfy.order.config.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述:
 * @author: 王毅
 * @date: 2019-01-12 18:49
 */
@ControllerAdvice
@Slf4j
public class OrderControllerAdvice {


    /**
     * @author: 王毅
     * @date: 2019-01-12 18:493
     * 功能描述:全局异常捕捉处理
     * 只要是有RequestMapping注解的方法都会被拦截
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public Map errorHandler(Exception ex) throws Exception {
        Map map = new HashMap();
        map.put("code", -1);
        map.put("msg", ex.getMessage());
        return map;
    }

    /**
     * @author: 王毅
     * @date: 2019-01-12 18:58
     * 功能描述:拦截捕捉自定义异常
     */
    @ResponseBody
    @ExceptionHandler(value = BusinessException.class)
    public Map myErrorHandler(BusinessException ex) {
        Map map = new HashMap();
        map.put("code", ex.getCode());
        map.put("msg", ex.getMsg());
        return map;
    }


}
