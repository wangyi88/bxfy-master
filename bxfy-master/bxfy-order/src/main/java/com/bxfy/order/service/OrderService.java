package com.bxfy.order.service;


import com.bxfy.order.model.Order;

/**
 * 功能描述: 
 * @param: 订单服务
 * @auther: 王毅
 * @date: 2019-01-12 18:18
 */
public interface OrderService {

    public Boolean CreatOrder(Order order);

}
