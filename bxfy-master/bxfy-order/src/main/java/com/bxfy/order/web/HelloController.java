package com.bxfy.order.web;


import com.alibaba.dubbo.config.annotation.Reference;
import com.bxfy.order.utils.ResultBean;
import com.bxfy.store.service.api.HelloServiceApi;
import com.bxfy.store.service.api.model.Order;
import com.bxfy.store.service.api.utils.PageList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 王毅
 */
@Slf4j
@RestController
public class HelloController {

    @Reference(version = "${demo.service.version}")
    private HelloServiceApi helloServiceApi;

    
    /**
     * @author 王毅
     * @date 2019/1/14 10:11
     * @description 
     */
    @RequestMapping("hello")
    public ResultBean<String> hello(@RequestParam("name") String name){
        return  new ResultBean<String>(helloServiceApi.sayHello(name));
    }

    /**
     * @author 王毅
     * @date 2019/1/14 10:10
     * @description 
     */
    @RequestMapping("testHello")
    public ResultBean<PageList<Order>> testHello(@RequestParam("name") String name){
        return  new ResultBean<PageList<Order>>(helloServiceApi.getOrderInfo(name));
    }

}
