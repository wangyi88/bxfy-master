package com.bxfy.order.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class Order  implements Serializable {

    private static final long serialVersionUID = -8225347065944154487L;

    private String orderId;

    private String orderType;

    private String cityId;

    private String platformId;

    private String userId;

    private String supplierId;

    private String goodsId;

    private String orderStatus;

    private String remark;

    private String createBy;

    private Date createTime;

    private String updateBy;

    private Date updateTime;

}