package com.bxfy.order.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.bxfy.order.constants.OrderStatus;
import com.bxfy.order.dao.OrderDAO;
import com.bxfy.order.model.Order;
import com.bxfy.order.service.OrderService;
import com.bxfy.store.service.api.StoreServiceApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.misc.Version;

import java.util.Date;
import java.util.UUID;

/**
 * @Auther: 王毅
 * @Date: 2019-01-12 18:20
 * @Description:
 */
@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDAO orderDAO;

    @Reference(version = "${demo.service.version}", retries = 0, timeout = 3000)
    private StoreServiceApi storeServiceApi;

    @Override
    @Transactional
    public Boolean CreatOrder(Order order) {
        boolean flag = true;
        try {
            order.setOrderId(UUID.randomUUID().toString().substring(0,30));
            order.setOrderType("1");
            order.setUserId("100137");
            order.setCityId("456789");
            order.setCreateBy("张三");
            order.setCreateTime(new Date());
            order.setSupplierId("00001");
            order.setGoodsId("123");
            order.setUpdateBy("李四");
            order.setUpdateTime(new Date());
            order.setOrderStatus(OrderStatus.ORDER_CREATED.getValue());
            order.setRemark("备注");
            //查询版本
            int currentVersion = storeServiceApi.selectVersion(order.getSupplierId(), order.getGoodsId());
            //修改库存  这个时候遇到错误了 解决办法是这个链接https://blog.csdn.net/fansili/article/details/78664267
            int updateRetCount = storeServiceApi.updateStoreCountByVersion(currentVersion, order.getGoodsId(), order.getUpdateBy(), order.getUpdateTime());
            if (updateRetCount == 1) {
                //如果出现SQL异常 入库失败，那么就要对库存的数量和版本号进行回滚操作
                orderDAO.insertSelective(order);
            }
            //没有更新成功 1高并发时乐观锁生效 2库存不足
            else if (updateRetCount == 0) {
                //下单标识失败
                flag = false;
                int currentStoreCount = storeServiceApi.selectStoreCount(order.getSupplierId(), order.getGoodsId());
                if (currentStoreCount == 0) {
                    System.out.println("当前库存不足");
                } else {
                    System.out.println("乐观锁生效");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            //具体捕获的异常是什么异常
            flag = false;
        }
        return flag;
    }

}
