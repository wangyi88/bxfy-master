package com.bxfy.order.config.exception;

import com.bxfy.order.utils.ResultBean;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: 王毅
 * @date: 2019-01-12 20:30
 * 功能描述: 自定义异常处理类
 */
@Slf4j
public class ControllerAop {

    /**
     * @author: 王毅
     * @date: 2019-01-13 0:24
     * 功能描述: aop切面处理方法
     */
    public Object handlerControllerMethod(ProceedingJoinPoint pjp) {
        long startTime = System.currentTimeMillis();
        ResultBean<?> result;
        try {
            result = (ResultBean<?>) pjp.proceed();
            log.info(pjp.getSignature() + "use time:"
                    + (System.currentTimeMillis() - startTime));
        } catch (Throwable e) {
            result = handlerException(pjp, e);
        }
        return result;
    }

    public ResultBean<?> handlerException(ProceedingJoinPoint pjp, Throwable e) {
        ResultBean<?> result = new ResultBean();
        /** jdk1.7 方式 */
//        String className="com.bxfy";
//        String fileName=".java";
//        String methodName="handlerControllerMethod";
//        List<StackTraceElement> stackTrasceElements = Arrays.asList(e.getStackTrace());
//        List<StackTraceElement> list= ListUtils.filter(stackTrasceElements, new ListFilterStrategy<StackTraceElement>() {
//            @Override
//            public boolean contain(StackTraceElement s) {
//                return s.getClassName().contains(className)&&s.getFileName().contains(fileName)&&!s.getMethodName().contains(methodName);
//            }
//        });
        List<StackTraceElement> list = Arrays.asList(e.getStackTrace()).stream()
                .filter((StackTraceElement se) ->
                        se.getClassName().contains("com.bxfy") && se.getFileName().contains(".java") && !se.getMethodName().contains("handlerControllerMethod"))
                .collect(Collectors.toList());
        if (e instanceof BusinessException) {
            result.setCode(((BusinessException) e).getCode());
            result.setMsg(((BusinessException) e).getMsg());
        } else {
            log.error(pjp.getSignature() + " error info is ", e);
            result.setCode(ResultBean.FAIL);
            result.setMsg(e.toString());
        }
        result.setErrorMsg(list.toString());
        log.info("报错的方法是" + list.toString());
        log.info("具体错误是" + e.toString());
        return result;
    }

}
