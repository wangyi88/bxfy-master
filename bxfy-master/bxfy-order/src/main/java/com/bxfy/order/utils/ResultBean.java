package com.bxfy.order.utils;

import java.io.Serializable;

/**
 * @author: 王毅
 * @date: 2019-01-12 18:57
 * 功能描述: 返回结果集实体类
 */
public class ResultBean<T> implements Serializable {
	
	private static final long serialVersionUID = 3776033949250520417L;

	/** 成功是0 */
	public static final int SUCCESS = 0;

	/** 失败是-1 */
	public static final int FAIL = -1;

	private int code = SUCCESS;

	private String msg = "success";

	private String errorMsg = null;

	private T data;

	public ResultBean() {
		super();
	}

	public ResultBean(T data) {
		super();
		this.data = data;
	}

	public ResultBean(Throwable e) {
		super();
		this.code = FAIL;
		this.msg = e.toString();
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public static int getSuccess() {
		return SUCCESS;
	}

	public static int getFail() {
		return FAIL;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
}
