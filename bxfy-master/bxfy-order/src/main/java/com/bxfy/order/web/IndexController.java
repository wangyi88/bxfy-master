package com.bxfy.order.web;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author 王毅
 * hystrix 断路器示范类
 */
@RestController
@Slf4j
public class IndexController {

    @RequestMapping("/index")
    public String toIndex(){
        return "index";
    }


    /**
     * 功能描述: 限流策略  超时方式
     * @param:
     * @return:
     * @auther: 王毅
     * @date: 2019-01-08 22:27
     */
    @HystrixCommand(
            commandKey = "/creatOrder",
            commandProperties = {
                    @HystrixProperty(name = "execution.timeout.enabled",value = "true"),
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "3000")
            },
            fallbackMethod = "createOrderFallbackMethod4Timeout"
    )
    @RequestMapping("/creatOrder")
    public String creatOrder(@RequestParam("orderId")String orderId)throws Exception{
        Thread.sleep(4000);
        return "下单成功";
    }

    public String createOrderFallbackMethod4Timeout(@RequestParam("orderId")String orderId)throws Exception{
        log.info("timeout 所以 超时降级方法执行");
        return "timeout";
    }


    /**
     * 功能描述: 限流策略 线程池方式
     * @auther: 王毅
     * @date: 2019-01-08 22:56
     */
    @HystrixCommand(
            commandKey = "/useOrder",
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.strategy",value = "THREAD"),
            },
            threadPoolKey = "useOrderThreadPool",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize",value = "10"),
                    @HystrixProperty(name = "maxQueueSize",value = "20000"),
                    @HystrixProperty(name = "queueSizeRejectionThreshold",value = "20")
            },
            fallbackMethod = "useOrderFallbackMethod4Thread"
    )
    @RequestMapping("/useOrder")
    @ResponseBody
    public String useOrder(@RequestParam("orderId")String orderId)throws Exception{
        return "下单成功";
    }

    public String useOrderFallbackMethod4Thread(@RequestParam("orderId")String orderId)throws Exception{
        log.info("---------线程池限流降级策略执行----------");
        return "hystrix threadpool !";
    }


    /**
     * 功能描述:限流方式 信号量方式
     * @auther: 王毅
     * @date: 2019-01-08 22:56
     * semephore
     */
    @HystrixCommand(
            commandKey = "useSemaphore",
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.strategy",value = "SEMAPHORE"),
                    @HystrixProperty(name = "execution.isolation.semaphore.maxConcurrentRequests",value = "3"),
            },
            fallbackMethod = "useSemaphoreFallbackMethod4Semaphore"
    )
    @RequestMapping("/useSemaphore")
    @ResponseBody
    public String useSemephore(@RequestParam("orderId")String orderId)throws Exception{
        return "下单成功";
    }

    public String useSemaphoreFallbackMethod4Semaphore(@RequestParam("orderId")String orderId)throws Exception{
        log.info("---------信号量限流降级策略执行----------");
        return "hystrix semephore !";
    }


}
