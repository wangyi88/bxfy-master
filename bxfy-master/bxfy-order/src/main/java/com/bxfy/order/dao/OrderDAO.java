package com.bxfy.order.dao;


import com.bxfy.order.model.Order;
import org.apache.ibatis.annotations.Mapper;

public interface OrderDAO {
    int deleteByPrimaryKey(String orderId);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(String orderId);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);
}