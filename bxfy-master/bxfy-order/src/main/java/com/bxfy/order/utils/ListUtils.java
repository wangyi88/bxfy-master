package com.bxfy.order.utils;

import com.bxfy.order.service.ListFilterStrategy;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 王毅
 * @date 2019-01-12 23:23:22
 * @description
 */
public class ListUtils {

    /**
     * 根据条件对List进行过滤
     * @param list
     * @param
     * @param <T>
     * @return
     */
    public static <T> List<T> filter(List<T> list, ListFilterStrategy<T> hook) {
        ArrayList<T> r = new ArrayList<T>();
        for (T t : list) {
            if (hook.contain(t)) {
                r.add(t);
            }
        }
        r.trimToSize();
        return r;
    }

}
