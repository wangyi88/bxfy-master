package com.bxfy.order.web;

import com.bxfy.order.model.Order;
import com.bxfy.order.service.OrderService;
import com.bxfy.order.utils.ResultBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author 王毅
 * @Date: 2019-01-08 22:17
 * @Description:
 */
@Slf4j
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping("/createOrder")
    public ResultBean<Boolean> createOrder(Order order){
        return  new ResultBean<Boolean>(orderService.CreatOrder(order));
    }



}
