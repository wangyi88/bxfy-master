package com.bxfy.order.config.hystrix;


import com.netflix.hystrix.contrib.javanica.aop.aspectj.HystrixCommandAspect;
import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: 王毅
 * @date: 2019-01-13 0:24
 * 功能描述:hystri断路器配置
 */
@Configuration
public class HystrixConfig {

    /**
     * @author: 王毅
     * @date: 2019-01-12 20:08
     * 功能描述:用来拦截处理HystrixCommand注解
     */
    @Bean
    public HystrixCommandAspect hystrixAspect(){
        return  new HystrixCommandAspect();
    }

    /**
     * @author: 王毅
     * @date: 2019-01-12 20:08
     * 功能描述:用来向监控中心Dashboard发送Stream信息
     */
    @Bean
    public ServletRegistrationBean hystrixMetricsStreamServlet(){
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new HystrixMetricsStreamServlet());
        registrationBean.addUrlMappings("/hystrix.stream");
        return registrationBean;
    }
}
