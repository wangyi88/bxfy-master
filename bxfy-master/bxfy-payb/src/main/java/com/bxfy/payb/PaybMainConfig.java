package com.bxfy.payb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author 王毅
 * 配置中心
 */
@Configuration
@ComponentScan(basePackages = {"com.bxfy.payb.*"})
@MapperScan(basePackages = {"com.bxfy.payb.mapper"})
public class PaybMainConfig {

}
