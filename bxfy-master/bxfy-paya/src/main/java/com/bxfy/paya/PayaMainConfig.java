package com.bxfy.paya;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author 王毅
 * 配置中心
 */
@Configuration
@ComponentScan(basePackages = {"com.bxfy.paya.*"})
@MapperScan(basePackages = {"com.bxfy.paya.dao"})
public class PayaMainConfig {

}
