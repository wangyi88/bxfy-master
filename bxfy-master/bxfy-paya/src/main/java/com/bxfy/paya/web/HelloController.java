package com.bxfy.paya.web;


import com.alibaba.dubbo.config.annotation.Reference;
import com.bxfy.store.service.api.HelloServiceApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 王毅
 */
@Slf4j
@RestController
public class HelloController {


    @Reference(version = "${demo.service.version}")
    private HelloServiceApi helloServiceApi;

    @RequestMapping("hello")
    public String hello(@RequestParam("name") String name)throws Exception{
        log.info("name is {} ",name);
        return  helloServiceApi.sayHello(name);
    }
}
