package com.bxfy.paya.config.datasource;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Properties;

/**
 * @author 王毅
 * druid 数据源配置
 */
@Component
@ConfigurationProperties(prefix = "spring.datasource")
@Data
public class DruidDataSourceSettings {

    private String driverClassName;

    private String url;

    private String username;

    private String password;

    private int initialSize;

    private int minIdle;

    private int maxActive;

    private long timeBetweenEvictionRunsMillis;

    private long minEvictableIdleTimeMillis;

    private String validationQuery;

    private boolean testWhileIdle;

    private boolean testOnBorrow;

    private boolean testOnReturn;

    private boolean poolPreparedStatements;

    private boolean useGlobalDataSourceStat;

    private int  maxPoolPrepareStatmentPerConnectionSize;

    private String  filters;

    private Properties connectionProperties;


}
