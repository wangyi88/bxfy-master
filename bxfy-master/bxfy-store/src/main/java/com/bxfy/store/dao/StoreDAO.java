package com.bxfy.store.dao;


import com.bxfy.store.model.Store;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

public interface StoreDAO {
    int deleteByPrimaryKey(String storeId);

    int insert(Store record);

    int insertSelective(Store record);

    Store selectByPrimaryKey(String storeId);

    int updateByPrimaryKeySelective(Store record);

    int updateByPrimaryKey(Store record);

    int selectVersion(@Param("supplierId") String supplierId,@Param("goodsId") String goodsId);

    int updateStoreCountByVersion(@Param("version") int version, @Param("goodsId") String goodsId, @Param("updateBy") String updateBy, @Param("updateTime") Date updateTime);

    int selectStoreCount(@Param("supplierId")String supplierId,@Param("goodsId") String goodsId);
}