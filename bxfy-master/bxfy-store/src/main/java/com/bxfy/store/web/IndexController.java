package com.bxfy.store.web;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author 王毅
 */
@RestController
public class IndexController {

    @RequestMapping("/index")
    public String toIndex(){
        return "index";
    }
}
