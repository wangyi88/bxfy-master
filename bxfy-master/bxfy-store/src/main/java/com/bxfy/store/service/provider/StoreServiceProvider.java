package com.bxfy.store.service.provider;

import com.alibaba.dubbo.config.annotation.Service;
import com.bxfy.store.dao.StoreDAO;
import com.bxfy.store.service.api.StoreServiceApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @author 王毅
 * @date 2019-01-13 13:32:37
 * @description
 */
@Service(version = "${demo.service.version}")
@Slf4j
public class StoreServiceProvider implements StoreServiceApi {

    @Autowired
    private StoreDAO storeDAO;

    @Override
    public int selectVersion(String supplierId, String goodsId) {
        return storeDAO.selectVersion(supplierId,goodsId);
    }

    @Override
    public int updateStoreCountByVersion(int version, String goodsId, String updateBy, Date updateTime) {
        return storeDAO.updateStoreCountByVersion(version,goodsId,updateBy,updateTime);
    }

    @Override
    public int selectStoreCount(String supplierId, String goodsId) {
        return storeDAO.selectStoreCount(supplierId,goodsId);
    }

}
