package com.bxfy.store.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class Store implements Serializable {

    private static final long serialVersionUID = -8913027981568952894L;

    private String storeId;

    private String goodsId;

    private String supplierId;

    private String goodsName;

    private Integer storeCount;

    private Integer version;

    private String createBy;

    private Date createTime;

    private String updateBy;

    private Date updateTime;

}