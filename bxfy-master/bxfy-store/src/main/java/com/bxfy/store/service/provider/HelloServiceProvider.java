package com.bxfy.store.service.provider;

import com.alibaba.dubbo.config.annotation.Service;
import com.bxfy.store.service.api.HelloServiceApi;
import com.bxfy.store.service.api.model.Order;
import com.bxfy.store.service.api.utils.PageList;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;


/**
 * @author 王毅
 * 实现新方法
 */
@Service(version = "${demo.service.version}")
@Slf4j
public class HelloServiceProvider implements HelloServiceApi {

    @Override
    public String sayHello(String name) {
        log.info("name is {}",name);
        return "hello"+name;
    }

    @Override
    public PageList<Order> getOrderInfo(String name) {
        Order order = new Order();
        order.setCityId("2344s");
        PageList<Order> orderPageList = new PageList<Order>();
        orderPageList.setLimit(10);
        orderPageList.setStart(0);
        orderPageList.setTotalCount(100L);
        orderPageList.setRoot(Arrays.asList(order));
        return orderPageList;
    }
}
