package com.bxfy.store;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author 王毅
 * 配置中心
 */
@Configuration
@ComponentScan(basePackages = {"com.bxfy.store.*"})
@MapperScan(basePackages = {"com.bxfy.store.mapper"})
public class StoreMainConfig {

}
