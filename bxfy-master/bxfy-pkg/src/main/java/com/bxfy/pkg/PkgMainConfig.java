package com.bxfy.pkg;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author 王毅
 * 配置中心
 */
@Configuration
@ComponentScan(basePackages = {"com.bxfy.pkg.*"})
@MapperScan(basePackages = {"com.bxfy.pkg.dao"})
public class PkgMainConfig {

}
