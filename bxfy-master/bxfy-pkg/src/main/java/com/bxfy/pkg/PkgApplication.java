package com.bxfy.pkg;


import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableDubbo
@SpringBootApplication
@MapperScan("com.bxfy.pkg.dao")
public class PkgApplication {

    public static void main(String[] args) {
        SpringApplication.run(PkgApplication.class,args);
    }

}

