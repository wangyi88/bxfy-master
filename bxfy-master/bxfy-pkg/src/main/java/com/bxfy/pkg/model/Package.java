package com.bxfy.pkg.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Package implements Serializable {

    private static final long serialVersionUID = 5866861042331968691L;

    private String packageId;

    private String orderId;

    private String supplierId;

    private String addressId;

    private String remark;

    private String packageStatus;

    private Date createTime;

    private Date updateTime;

}