package com.bxfy.pkg.config.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @author 王毅
 * druid 数据源配置
 */
@Configuration
@EnableTransactionManagement
@Slf4j
public class DruidDataSourceConfig  {

    @Autowired
    private DruidDataSourceSettings druidSettings;

    private static String DRIVER_CLASSNAME;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public ServletRegistrationBean druidServlet(){
        ServletRegistrationBean reg = new ServletRegistrationBean();
        reg.setServlet(new StatViewServlet());
        reg.addUrlMappings("/druid/*");
        reg.addInitParameter("allow","localhost");
        reg.addInitParameter("deny","/deny");
        log.info("druid console manager init :{}",reg);
        return reg;
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean(){
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new WebStatFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.addInitParameter("exclusions","*.js,*.gif,*.jpg,*.png,*.css");
        log.info(" druid filter register :{}",filterRegistrationBean);
        return filterRegistrationBean;
    }

    @Bean
    public DataSource dataSource() throws SQLException {
        DruidDataSource dds = new DruidDataSource();
        DRIVER_CLASSNAME=druidSettings.getDriverClassName();
        dds.setDriverClassName(DRIVER_CLASSNAME);
        dds.setUrl(druidSettings.getUrl());
        dds.setUsername(druidSettings.getUsername());
        dds.setPassword(druidSettings.getPassword());
        dds.setInitialSize(druidSettings.getInitialSize());
        dds.setMinIdle(druidSettings.getMinIdle());
        dds.setMaxActive(druidSettings.getMaxActive());
        dds.setTimeBetweenEvictionRunsMillis(druidSettings.getTimeBetweenEvictionRunsMillis());
        dds.setMinEvictableIdleTimeMillis(druidSettings.getMinEvictableIdleTimeMillis());
        dds.setValidationQuery(druidSettings.getValidationQuery());
        dds.setTestOnBorrow(druidSettings.isTestOnBorrow());
        dds.setTestOnReturn(druidSettings.isTestOnReturn());
        dds.setPoolPreparedStatements(druidSettings.isPoolPreparedStatements());
        dds.setMaxPoolPreparedStatementPerConnectionSize(druidSettings.getMaxPoolPrepareStatmentPerConnectionSize());
        dds.setFilters(druidSettings.getFilters());
        dds.setConnectProperties(druidSettings.getConnectionProperties());
        log.info("druid datasource is {}",dds);
        return dds;
    }

    /**
     * 事务控制
     * @return
     * @throws SQLException
     */
    @Bean
    public PlatformTransactionManager transactionManager() throws SQLException {
        DataSourceTransactionManager txmanager=new DataSourceTransactionManager();
        txmanager.setDataSource(dataSource());
        return txmanager;
    }
}
