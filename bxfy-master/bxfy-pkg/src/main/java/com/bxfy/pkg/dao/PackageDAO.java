package com.bxfy.pkg.dao;

import com.bxfy.pkg.model.Package;

public interface PackageDAO {
    int deleteByPrimaryKey(String packageId);

    int insert(Package record);

    int insertSelective(Package record);

    Package selectByPrimaryKey(String packageId);

    int updateByPrimaryKeySelective(Package record);

    int updateByPrimaryKey(Package record);
}